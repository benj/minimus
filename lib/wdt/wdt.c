/* Library for controlling the Watchdog timer
 */

#include "avr/io.h"
#include "avr/wdt.h"
#include "../lib.h"

/*! Function that disables the Watchdog Timer */
void wdtDsb() {
   MCUSR &= ~(1 << WDRF); 
   wdt_disable();
}
