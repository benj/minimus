#include "avr/io.h"
#include "avr/interrupt.h"
#include "irq.h"
#include "../rot/rot.h"


//Function for enabling/disabing any interrupt.
void setIrqMod(int irq, int tpe, int mod) {
   //Enable global interrupt 
   //Get the fields of the interrupt.
   int irqNum = ((int)irq) & 0xF; 
   int irqGrp = ((irq >> 8)) & 0xF;
   
   // Enable the global interrupt.
   sei();
   
   //Set the sense of the interrupt.
   if (mod == ENB) {
      //Check if the interrupt is generic or pin-change.
      if (irqGrp == (GEN >> 8)) {
         //Set the sense of the interrupt 
         if (irqNum < 4) {
            SETBITVAL(EICRA, ((2 * irqNum) + 0), (tpe & 0b1)); 
            SETBITVAL(EICRA, ((2 * irqNum) + 1), ((tpe & 0b10) >> 1)); 
         }
         else {
            SETBITVAL(EICRB, ((2 * (irqNum - 4)) + 0), (tpe & 0b1)); 
            SETBITVAL(EICRB, ((2 * (irqNum - 4)) + 1), ((tpe & 0b10) >> 1)); 
         }
         //Set the mode of the interrupt the mask of the interrupt.
         SETBITVAL(EIMSK, irqNum, mod);
      }
      else if (irqGrp == (PCH >> 8)) {
         if (irqNum < 8) {
            SETBITVAL(PCICR, PCIE0, mod);
            SETBITVAL(PCMSK0, irqNum, mod);
         }
         else {
            SETBITVAL(PCICR, PCIE1, mod);
            SETBITVAL(PCMSK1, (irqNum - 8), mod);
         }
      }
   }
}
