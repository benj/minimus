// ****** Source code for Interrupt handling ****** //
//Interrupt mode
#define ENB 1
#define DSB 0

//Seperation of interrupts to generics and pin change.
#define GEN 0x100
#define PCH 0x200

//Definition of generic irq sense
#define LOW     0
#define EDG_ANY 1
#define EDG_RIS 2
#define EDG_FAL 3

//Mapping of the interrupts to ports
////Port B Interrupts
#define PCPB7 PCH + 0xB0 + PCINT7
#define PCPB6 PCH + 0xB0 + PCINT6
#define PCPB5 PCH + 0xB0 + PCINT5
#define PCPB4 PCH + 0xB0 + PCINT4
#define PCPB3 PCH + 0xB0 + PCINT3
#define PCPB2 PCH + 0xB0 + PCINT2
#define PCPB1 PCH + 0xB0 + PCINT1
#define PCPB0 PCH + 0xB0 + PCINT0
////Port C Interrupts
#define PIPC7 GEN + 0xC0 + INT7 ??
#define PCPC6 PCH + 0xC0 + PCINT8
#define PCPC5 PCH + 0xC0 + PCINT9
#define PCPC4 PCH + 0xC0 + PCINT10
#define PCPC2 PCH + 0xC0 + PCINT11
////Port D Interrupts
#define PIPD7 GEN + 0xD0 + INT7
#define PIPD6 GEN + 0xD0 + INT6
#define PIPD5 GEN + 0xD0 + INT5
#define PIPD4 GEN + 0xD0 + INT4
#define PIPD3 GEN + 0xD0 + INT3
#define PIPD2 GEN + 0xD0 + INT2
#define PIPD1 GEN + 0xD0 + INT1
#define PIPD0 GEN + 0xD0 + INT0

//Function for enabling/disabing any interrupt.
void setIrqMod(int irq, int tpe, int mod);

//Function for enabling a generic interrupt
#define setGenIrq(irq, tpe) setIrqMod(irq, mod, ENB)

//Function for disabling a generic interrupt
#define clrGenIrq(irq, tpe) setIrqMod(irq, mod, DSB)

//Function for enabling a change interrupt
#define setChnIrq(irq) setIrqMod(irq, 0, ENB)

#define clrChnIrq(irq) setIrqMod(irq, 0, DSB)
