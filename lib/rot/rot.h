/* ****** Root Section ****** */

/*! A function that sets a bit of a register to a value
 *  The bit takes values [1 ... 8]
 *  The val takes values [0 ... 1]
 */
#define SETBIT(ADDRESS, BIT) (ADDRESS |= (1<<BIT))
#define CLRBIT(ADDRESS, BIT) (ADDRESS &= ~(1<<BIT))
#define SETBITVAL(ADDRESS, BIT, VAL) \
   if (VAL == 1) \
      SETBIT(ADDRESS, BIT); \
   else \
      CLRBIT(ADDRESS, BIT); 
#define TGLBIT(ADDRESS, BIT) (ADDRESS ^= (1<<BIT))

/* Macro for getting the bit of a register.*/
#define GETBITVAL(ADDRESS, BIT) ((ADDRESS >> BIT) & 0x1)

/*! Definitions for success and fail.*/
#define SCS       0 /*!< Definition of success.*/
#define FAL       1 /*!< Definition of fail.*/
#define ERRNOMEM  2 /*!< Fail message for trying to allocate a slot that has already been allocated.*/
#define ERRINVADR 3 /*!< Fail message for trying to allocate a slot that has already been allocated.*/
#define ERRINVDAT 4 /*!< Fail message for trying to allocate a slot that has already been allocated.*/
