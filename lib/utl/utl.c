// ===========================================================================
//  File          :    utl.c
//  Entity        :    utl
//  Purpose       :    Utility Library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Aug-2012
//  File history    :
// ===========================================================================

#include "util/delay.h"

/*! A function for delay. The optimizations are turned on
 *   since they are demanded from the _delay* function. */
#pragma GCC push_options
#pragma GCC optimize("O3")
//void del_us(int micSec) {
//	_delay_us(micSec);
//}
//
//void del_ms(int milSec) {
//	_delay_ms(milSec);
//}
#pragma GCC pop_options
