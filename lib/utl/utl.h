// ===========================================================================
//  File          :    utl.c
//  Entity        :    utl
//  Purpose       :    Utility Library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Aug-2012
//  File history    :
// ===========================================================================

/*! Delay functions. */
void del_us(int micSec);
void del_ms(int milSec);
