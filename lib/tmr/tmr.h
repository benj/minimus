// ===========================================================================
//  File          :    tmr.h
//  Entity        :    tmr
//  Purpose       :    Timers control library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

// Timers GPIO
#define TMR_OUT_A GPIO_B_ADD + 7
#define TMR_OUT_B GPIO_D_ADD + 1

// Timer compare output
#define CMP_OUT_A 0
#define CMP_OUT_B 1

// Waveform generation
#define MOD_NORMAL            0
#define MOD_PWM_PHS_CRR       1
#define MOD_CTC               2
#define MOD_PWM_FST           3
#define MOD_PWM_PHS_CRR_OCRA  4
#define MOD_PWM_FST_OCRA      5

// Timer selection
#define TMR_CLK_DSC     0
#define TMR_CLK         1
#define TMR_CLK8        2
#define TMR_CLK64       3
#define TMR_CLK256      4
#define TMR_CLK1024     5
#define TMR_CLKEXT_FAL  6
#define TMR_CLKEXT_RIS  7

#define TMR_INT_ENB 1
#define TMR_INT_DSB 0

// A macro for resetting the timers
#define rstTmr() SETBIT(GTCCR, PSRSYNC);

// A macro for enabling the timer compare output
#define enbTmrOut(out)\
   if (out == CMP_OUT_A) {\
      gpioSetDir(TMR_OUT_A, OUT);\
   }\
   else if (out == CMP_OUT_B) {\
      gpioSetDir(TMR_OUT_B, OUT);\
   }

// A macro for toggling the output of timer A on a match
#define togTmrOut(out)\
   if (out == CMP_OUT_A) {\
      SETBITVAL(TCCR0A, COM0A1, 0);\
      SETBITVAL(TCCR0A, COM0A0, 1);\
   }\
   else {\
      SETBITVAL(TCCR0A, COM0B1, 0);\
      SETBITVAL(TCCR0A, COM0B0, 1);\
   }\

// A macro for clearing the output of timer A on a match
#define clrTmrOut(out)\
   if (out == CMP_OUT_A) {\
      SETBITVAL(TCCR0A, COM0A0, 0);\
      SETBITVAL(TCCR0A, COM0A1, 1);\
   }\
   else {\
      SETBITVAL(TCCR0A, COM0B0, 0);\
      SETBITVAL(TCCR0A, COM0B1, 1);\
   }\
}

// A macro for setting the output of timer A on a match
#define setTmrOut(out)\
   if (out == CMP_OUT_A) {\
      SETBITVAL(TCCR0A, COM0A0, 1);\
      SETBITVAL(TCCR0A, COM0A1, 1);\
   }\
   else {\
      SETBITVAL(TCCR0A, COM0B0, 1);\
      SETBITVAL(TCCR0A, COM0B1, 1);\
   }

// A macro for disconnecting the output of timer A
#define dscTmrOut(out)\
   if (out == CMP_OUT_A) {\
      SETBITVAL(TCCR0A, COM0A0, 0);\
      SETBITVAL(TCCR0A, COM0A1, 0);\
   }\
   else {\
      SETBITVAL(TCCR0A, COM0B0, 0);\
      SETBITVAL(TCCR0A, COM0B1, 0);\
   }

//Function for selecting the clock source of the timer
#define setTmrClk(src) \
   switch (src) {\
      case TMR_CLK_DSC: {\
         SETBITVAL(TCCR0B, CS02, 0);\
         SETBITVAL(TCCR0B, CS01, 0);\
         SETBITVAL(TCCR0B, CS00, 0);\
         break;\
      }\
      case TMR_CLK: {\
         SETBITVAL(TCCR0B, CS02, 0);\
         SETBITVAL(TCCR0B, CS01, 0);\
         SETBITVAL(TCCR0B, CS00, 1);\
         break;\
      }\
      case TMR_CLK8: {\
         SETBITVAL(TCCR0B, CS02, 0);\
         SETBITVAL(TCCR0B, CS01, 1);\
         SETBITVAL(TCCR0B, CS00, 0);\
         break;\
      }\
      case TMR_CLK64: {\
         SETBITVAL(TCCR0B, CS02, 0);\
         SETBITVAL(TCCR0B, CS01, 1);\
         SETBITVAL(TCCR0B, CS00, 1);\
         break;\
      }\
      case TMR_CLK256: {\
         SETBITVAL(TCCR0B, CS02, 1);\
         SETBITVAL(TCCR0B, CS01, 0);\
         SETBITVAL(TCCR0B, CS00, 0);\
         break;\
      }\
      case TMR_CLK1024: {\
         SETBITVAL(TCCR0B, CS02, 1);\
         SETBITVAL(TCCR0B, CS01, 0);\
         SETBITVAL(TCCR0B, CS00, 1);\
         break;\
      }\
      case TMR_CLKEXT_RIS: {\
         SETBITVAL(TCCR0B, CS02, 1);\
         SETBITVAL(TCCR0B, CS01, 1);\
         SETBITVAL(TCCR0B, CS00, 1);\
         break;\
      }\
      case TMR_CLKEXT_FAL: {\
         SETBITVAL(TCCR0B, CS02, 1);\
         SETBITVAL(TCCR0B, CS01, 1);\
         SETBITVAL(TCCR0B, CS00, 1);\
         break;\
      }\
   }

//A macro for returning the counter value
#define getCntVal() \
   TCNT0 + 0;\

//A macro for setting the counter value
#define setCntVal( val ) \
   TCNT0 = val;

//A macro for setting the output compare value 
#define setCmpVal( out, val ) \
   if (out == CMP_OUT_A)\
      OCR0A = val;\
   else\
      OCR0B = val;

//A macro for setting the output compare value 
#define getCmpVal( out, val ) \
   if (out = CMP_OUT_A)\
      OCR0A + 0;\
   else\
      OCR0B + 0;

//A macro for enabling the timer's interrupt
#define setTmrInt( out , enb ) \
   if ( out == CMP_OUT_A) \
      SETBITVAL(TIMSK0, OCIE0A, enb ); \
   else\
      SETBITVAL(TIMSK0, OCIE0B, enb );

//A macro for enabling the timer's interrupt on the overflow event
#define setOvrTmrInt( out , enb ) \
   SETBITVAL( TIMSK0, TOIE, enb );

//Function for selecting the waveform generation mode
#define setWvfMod(mod) \
   switch (mod) {\
      case MOD_NORMAL: {\
         SETBITVAL(TCCR0A, WGM00, 0);\
         SETBITVAL(TCCR0A, WGM01, 0);\
         SETBITVAL(TCCR0B, WGM02, 0);\
         break;\
      }\
      case MOD_PWM_PHS_CRR: {\
         SETBITVAL(TCCR0A, WGM00, 1);\
         SETBITVAL(TCCR0A, WGM01, 0);\
         SETBITVAL(TCCR0B, WGM02, 0);\
         break;\
      }\
      case MOD_CTC: {\
         SETBITVAL(TCCR0A, WGM00, 0);\
         SETBITVAL(TCCR0A, WGM01, 1);\
         SETBITVAL(TCCR0B, WGM02, 0);\
         break;\
      }\
      case MOD_PWM_FST: {\
         SETBITVAL(TCCR0A, WGM00, 1);\
         SETBITVAL(TCCR0A, WGM01, 1);\
         SETBITVAL(TCCR0B, WGM02, 0);\
         break;\
      }\
      case MOD_PWM_PHS_CRR_OCRA: {\
         SETBITVAL(TCCR0A, WGM00, 1);\
         SETBITVAL(TCCR0A, WGM01, 0);\
         SETBITVAL(TCCR0B, WGM02, 1);\
         break;\
      }\
      case MOD_PWM_FST_OCRA: {\
         SETBITVAL(TCCR0A, WGM00, 1);\
         SETBITVAL(TCCR0A, WGM01, 1);\
         SETBITVAL(TCCR0B, WGM02, 1);\
         break;\
      }\
   }
#define enbTmrInt(out)\
   if (out == CMP_OUT_A) {\
      SETBIT(TIMSK0, OCIE0A);\
      SETBIT(TIFR0,  OCF0A);\
   }\
   else {\
      SETBIT(TIMSK0, OCIE0B);\
      SETBIT(TIFR0,  OCF0B);\
   }

