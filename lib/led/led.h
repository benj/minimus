// ===========================================================================
//  File          :    led.c
//  Entity        :    led
//  Purpose       :    LED library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Aug-2012
//  File history    :
// ===========================================================================

#include "../gpio/gpio.h"

#define LED_GPIO_ADD GPIO_D_ADD                 /*!< LED GPIO address.*/ 
#define LED_BLU_GPIO_FLD 5                      /*!< GPIO bit associated with the red  LED.*/
#define LED_RED_GPIO_FLD 6                      /*!< GPIO bit associated with the blue LED.*/
#define LED_BLU LED_GPIO_ADD + LED_BLU_GPIO_FLD /*!< Compact address for blue LED.*/
#define LED_RED LED_GPIO_ADD + LED_RED_GPIO_FLD /*!< Compact address for red  LED.*/

#define LED_ON 0
#define LED_OFF 1

//Macro for initializing an LED.
#define ledIni(LED) \
   gpioSetVal(LED, LED_OFF);\
   gpioSetDir(LED, OUT);

//Macro for setting an LED.
#define ledSet(LED)\
   gpioClr(LED);

//Macro for clearing an LED.
#define ledClr(LED)\
   gpioSet(LED);

//Macro for Setting a LED.
#define ledSetVal(LED, val)\
   gpioSetVal(LED, val);

//Macro for toggling an LED.
#define ledTgl(LED) \
   gpioTgl(LED)

//Macro for toggling the LED  
//with an intermediate delay.
#define TGLLEDEFF(LED, DEL) \
   ledSet(LED); \
   del(DEL);    \
   ledClr(LED); \
   del(DEL);
