// ===========================================================================
//  File          :    cdlPub.h
//  Entity        :    cdlPub
//  Purpose       :    Character Display LCD Library module public header.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

/*! Initialization function */
void cdlIni();

/*! Command function. */
void cdlCtl();

/*! Data write function */
void cdlWr();
