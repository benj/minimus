// ===========================================================================
//  File          :    cdlPrv.h
//  Entity        :    cdl
//  Purpose       :    Character Display LCD Library private header.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

#include "../gpio/gpio.h"

// Connections.
#define CDL_NC_PRT 0xB0 + 4  /*!< Port connected with the NC */
#define CDL_RS_PRT 0xB0 + 5  /*!< Port connected with the RS */
#define CDL_D4_PRT 0xB0 + 6  /*!< Port connected with the D4 */
#define CDL_D5_PRT 0xC0 + 6  /*!< Port connected with the D5 */
#define CDL_D6_PRT 0xB0 + 7  /*!< Port connected with the D6 */
#define CDL_D7_PRT 0xC0 + 5  /*!< Port connected with the D7 */
#define CDL_E_PRT  0xC0 + 7  /*!< Port connected with the E  */

// Port control.
#define cdlSetNc gpioSetVal(GPIO_B_ADD + 4); /*!< Macro for setting NC */
#define cdlSetRs gpioSetVal(GPIO_B_ADD + 5); /*!< Macro for setting RS */
#define cdlSetD4 gpioSetVal(GPIO_B_ADD + 6); /*!< Macro for setting D4 */
#define cdlSetD6 gpioSetVal(GPIO_B_ADD + 7); /*!< Macro for setting D5 */
#define cdlSetE  gpioSetVal(GPIO_C_ADD + 7); /*!< Macro for setting D6 */
#define cdlSetD5 gpioSetVal(GPIO_C_ADD + 6); /*!< Macro for setting D7 */
#define cdlSetD7 gpioSetVal(GPIO_C_ADD + 5); /*!< Macro for setting E  */

// Commands.
#define CDL_COM_CLR         0x0        /*!< Display Clear command  1 location*/
#define CDL_COM_CUR_HOM     0x1        /*!< Display/Cursor Home command  1 location*/
#define CDL_COM_SET_CUR_DIR 0x2        /*!< Entry Mode Set command  1 location*/
#define CDL_COM_POW         0x3        /*!< Display ON/OFF command  1 location*/
#define CDL_COM_SHF_CUR     0x4        /*!< Display Cursor Shift command  1 location*/
#define CDL_COM_SET_FUN     0x5        /*!< Function Set command  1 location*/
#define CDL_COM_SET_ADR     0x6        /*!< RAM Address SET command  1 location*/
#define CDL_COM_SET_DAT_ADR 0x7        /*!< Data RAM Address set command  1 location*/
#define CDL_COM_RD_BFL_ADD  0x8        /*!< Busy Flag Address read command  1 location*/
#define CDL_COM_WR_CNT_DAT  0x9        /*!< Write data to   control or data RAM command  1 location*/
#define CDL_COM_RD_CNT_DAT  0x90 + 0x8 /*!< Read  data from control or data RAM command  1 locations*/

//// Command's execution time in microseconds.
#define CDL_COM_TME_CLR         1640 /*!< Display Clear command  execution time*/
#define CDL_COM_TME_CUR_HOM     1640 /*!< Display/Cursor Home command  execution time*/
#define CDL_COM_TME_SET_CUR_DIR   40 /*!< Entry Mode Set command  execution time*/
#define CDL_COM_TME_POW           40 /*!< Display ON/OFF command  execution time*/
#define CDL_COM_TME_SHF_CUR       40 /*!< Display Cursor Shift command  execution time*/
#define CDL_COM_TME_SET_FUN       40 /*!< Function Set command  execution time*/
#define CDL_COM_TME_SET_ADR       40 /*!< RAM Address SET command  execution time*/
#define CDL_COM_TME_SET_DAT_ADR   40 /*!< Data RAM Address set command  execution time*/
#define CDL_COM_TME_RD_BFL_ADD    40 /*!< Busy Flag Address read command  execution time*/
#define CDL_COM_TME_WR_CNT_DAT    40 /*!< Write data to   control or data RAM command  execution time*/
#define CDL_COM_TME_RD_CNT_DAT    40 /*!< Read  data from control or data RAM command  execution times*/

//// Commands' fields.
#define CDL_COM_FLD_SHF 0         /*!<Command's field for display shift.*/ 
#define CDL_COM_FLD_DIR 1         /*!<Command's field for cursor direction. */
#define CDL_COM_FLD_BLK 0         /*!<Command's field for cursor blinking mode. */
#define CDL_COM_FLD_POW_DSP 2     /*!<Command's field for display power mode. */
#define CDL_COM_FLD_POW_BLK_CRS 1 /*!<Command's field for cursor  power mode. */
#define CDL_COM_FLD_POW_CLK_CHR 0 /*!<Command's field for character power mode. */
#define CDL_COM_FLD_SHF_ENB 2     /*!<Command's field for enabling shifting. */
#define CDL_COM_FLD_SHF_DIR 1     /*!<Command's field for shifting direction. */
#define CDL_COM_FLD_DSP_FNT 2     /*!<Command's field for font */
#define CDL_COM_FLD_DSP_LNE 3     /*!<Command's field for lines number selection. */

////// Commands' fields' valules.
#define CDL_COM_FLD_VAL_DIR_LFT 1
#define CDL_COM_FLD_VAL_DIR_RGT 0
#define CDL_COM_FLD_VAL_SHF_ON  1
#define CDL_COM_FLD_VAL_SHF_OFF 0
#define CDL_COM_FLD_VAL_BLK_ON  1
#define CDL_COM_FLD_VAL_BLK_OFF 0
#define CDL_COM_FLD_VAL_SHF_RGT 1
#define CDL_COM_FLD_VAL_SHF_LFT 0
#define CDL_COM_FLD_VAL_FUN_4BT 1
#define CDL_COM_FLD_VAL_FUN_8BT 0
#define CDL_COM_FLD_VAL_FUN_DLN 1
#define CDL_COM_FLD_VAL_FUN_SLN 0
#define CDL_COM_FLD_VAL_FUN_10D 1
#define CDL_COM_FLD_VAL_FUN_08D 0
#define CDL_COM_FLD_VAL_BSY     1
#define CDL_COM_FLD_VAL_IDL     0
