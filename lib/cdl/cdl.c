// ===========================================================================
//  File          :    cdl.c
//  Entity        :    cdl
//  Purpose       :    Character Display Module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Aug-2012
//  File history    :
// ===========================================================================

#include "cdlPrv.h"
#include "cdlPub.h"
#include "util/delay.h"

/*! CDL Initialization function. */
void cdlIni(int dis, int crs, int blk, int inc, int shf) {

   char txWrd;
   /* Set the ports connected with the CLD as outputs */
   gpioSetDir(GPIO_B_ADD + 4, OUT);
   gpioSetDir(GPIO_B_ADD + 5, OUT); 
   gpioSetDir(GPIO_B_ADD + 6, OUT);
   gpioSetDir(GPIO_B_ADD + 7, OUT);
   gpioSetDir(GPIO_C_ADD + 7, OUT);
   gpioSetDir(GPIO_C_ADD + 6, OUT);
   gpioSetDir(GPIO_C_ADD + 5, OUT);

   // The following comments have been extracted from the NT7605 datasheet, page 25.
   //// Power On
   //// Wait for more that 30 ms after VDD on.
   __delay_ms(40);

   //// Function Set.
   txWrd = 0;
   cdlTx(txWrd);

   txWrd = 0b001000;
   txWrd != (dsp << CDL_DSP) | (crs << CDL_CRS) | (blk << CDL_BLK);
   cdlTx(txWrd);
   
   //// Wait for more than 40 us
   __delay_us(50);
   txWrd = 0;
   cdlTx(txWrd);

   txWrd = 1;
   cdlTx(txWrd);

   //// Wait for more than 1.64ms
   __delay_ms(2);
   txWrd = 0;
   cdlTx(txWrd);

   txWrd = (inc << CDL_INC) | (shf << CDL_SHF);
   cdlTx(txWrd);

   //// Initialization end.
   txWrd = 0b100100;
   cdlTx(txWrd);

   txWrd = 0b101110;
   cdlTx(txWrd);
}
