// ===========================================================================
//  File          :    scd.h
//  Entity        :    scd
//  Purpose       :    
//  Documentation :   
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 19-Aug-2012
// ===========================================================================
/*! \struct scdSltAtrStr
 */
struct scdSltAtrStr {
   char brk; /*!< Breakable, indicates that the slot can be interrupted and restarted.*/
}

struct scdSltAtrStr scdSltAtr_t;
/*! \struct scdSltFunStr
 * Structure used by each driver, in order to declare each basic functions.
 */
struct scdSltFunStr {
   (int  *) read(char *dat, int len);
   (int  *) write(char *dat, int len);
   (char *) ioctl(char *req, char reqDat, char reqNum);
};

typedef struct scdSltFunStr scdSltFun_t;


/* \struct scdDrvStr
 * Structure used by each slot (driver/program).  
 */
struct scdSltStr {
   scdSltFun_t fun;     /*!< Functions .*/
   scdSltAtr_t atr;     /*!< Attributes.*/
   char        alc = 0; /*!< Allocated flag.*/
   char        bsy = 0; /*!< Busy flag.*/
   char        enb = 0; /*!< Enabled/Disabled flag.*/
   int         pc;      /*!< Value of the program counter before the break.*/
   
};

typedef struct scdSltStr scdSlt_t;

/*! Function implementing write system call.*/
int scdRead(char *dat, int len, int idf);

/*! Function implementing read system call.*/
int scdWrite(char *dat, int len, int idf);

/*! Function implementing ioctl system call.*/
char scdIoctl(char *req, char reqNum, int idf);

/*! Function for inserting a new slot.*/
char scdAddSlt(scdSlt_t slt, int idf);

/*! Function for inserting a new slot.*/
char scdRmvSlt(scdSlt_t slt, int idf);

/*! Function for initializing the scheduler.*/
char scdIni();
