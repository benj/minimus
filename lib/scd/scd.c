// ===========================================================================
//  File          :    scd.c
//  Entity        :    scd
//  Purpose       :    Scheduler of the operating system.
//  Documentation :   
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 19-Aug-2012
// ===========================================================================

#include "scd.h"
#define SCD_SLT_NUM 2 /*!< Definition of the number of slots.*/

scdSlt_t slt[SCD_SLT_NUM]; /*!< Table of pointers towards the slots.*/

#ifdef SCD
#   ifdef TMR0_COMPA_IRQ
#     warning "ISR for TIMER0_COMPA_vect has been defined multiple times."
#  else
#     define TMR0_COMPA_IRQ
#  endif
ISR(TIMER0_COMPA_vect) {
   scdArb(); 
}
#endif

/*! Function for initializing the scheduler.*/
char scdIni(int per) {
   if (per < 255)
      setTmrClk(TMR_CLK);
      setCmpVal((char)per);
   }
   else if ((per/8) <= 255) {
      setTmrClk(TMR_CLK8);
      setCmpVal((char)(per/8));
   }
   else if ((per/64) <= 255) {
      setTmrClk(TMR_CLK64);
      setCmpVal((char)(per/64));
   else if ((per/256) <= 255) {
      setTmrClk(TMR_CLK256);
      setCmpVal((char)(per/256));
   }
   else if ((per/1024) <= 255) {
      setTmrClk(TMR_CLK1024);
      setCmpVal((char)(per/1024));
   }
   else 
      return ERRINVDAT;
   enbTmrInt(CMP_OUT_A);
   sei();

   return SUC;
}

/*! Function for inserting a new slot.*/
char scdAddSlt(scdSlt_t *newSlt, char idf) {

   if (slt[idf].alc == 1)
      return ERRNOMEM;
   else
      slt[idf].alc = 1;
   
   if (newSlt->fun == NULL) {
      return ERRINVADR;
   
   slt[idf].fun     = newSlt->fun;
   slt[idf].atr.brk = newSlt->atr.brk;
   slt[idf].atr.enb = newSlt->enb;
   return SUC;
}

/*! Function for removing a slot.*/
char scdRmvSlt(char idf) {

   if (slt[idf].alc == 0)
      return ERRINVADR;
   else
      slt[idf].alc = 0;

   return SUC;
}

/*! Function for implementing ioctl system call.*/
int scdIoctl(char *req, char *reqNum, int idf) {

   int idx;
   int res;

   if (slt[idf].alc == 0) 
      return ERRINVADR;

   if (slt[idf].bsy == 0)
      return ERRRETRY;
   else
      for (reqIdx = 0; reqIdx <reqNum; reqIdx++)
         res = slt[idf].fun.ioctl(req[idx], reqDat);

   return res;
}

/*! Function for implementing write system call.*/
int scdWrite(char *dat, int len, int idf) {
   if (slt[idf].alc == 0) 
      return ERRINVADR;

   if (slt[idf].bsy == 0)
      return ERRRETRY;
   else
      res = slt[idf].fun.write(dat, len);
}

/*! Function for implementing read system call.*/
int scdWrite(char *dat, int len, int idf) {
   if (slt[idf].alc == 0) 
      return ERRINVADR;

   if (slt[idf].bsy == 0)
      return ERRRETRY;
   else
      res = slt[idf].fun.read(dat, len);
}

void scdArb() {
   static int sltIdx = 0; 
   int lstSltIdx = sltIdx;

   sltIdx++;

   while (sltIdx != lstSltIdx) {
      if (slt[sltIdx].bsy = 1) {
         //Code needed here.
      }
      else {
         if (sltIdx == SCD_SLT_NUM)
            sltIdx = 0;
         else
            sltIdx++;
      }
   }

   return;
}
