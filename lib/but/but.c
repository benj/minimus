/*! Function for enabling the button-generated interrupt 
 *  The interrupt will be generated at a low-level 
 */
#include "but.h"
#include "avr/interrupt.h"
#include "../rot/rot.h"
#include "util/delay.h"

#define BUT_RPL_TME 1000

void butEnbIrq() {
   //Setup the external button interrupt.
   ////Enable the global-interrupt gate.
   sei();
   ////Set the condition of the interrupt to be the low-level
   EICRB = 0;
   ////Enable the button interrupt
   SETBIT(EIMSK, 7);
}

/*! Function for waiting until there is 
 *  a clear button press 
 */
void butPol() {
   // Wait until the button had been pressed.
   while(!butGetVal());

   // Wait for some time to get away from glitches
   _delay_us(BUT_RPL_TME);
}


/*! Function for getting the button pressed event filtered.
 */
int butGetCln() {
   int ret = butGetVal();
   
   // Clean the button press from noise.
   if (ret) {
      _delay_us(BUT_RPL_TME);
   }
   return ret;
}
