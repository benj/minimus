// ===========================================================================
//  File          :    but.h
//  Entity        :    but
//  Purpose       :    Button library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

#include "../gpio/gpio.h"

#define BUT_GPIO_ADD GPIO_D_ADD               /*!< GPIO Register associated with the button.*/
#define BUT_GPIO_FLD 7                        /*!< GPIO Register bit associated with the button.*/
#define BUT_ADD BUT_GPIO_ADD + BUT_GPIO_FLD   /*!< GPIO Compact address for the button.*/

#define butIni()\
   gpioSetVal(BUT_ADD, 0); \
   gpioSetDir(BUT_ADD, IN);\

#define butGetVal() \
  !gpioGetVal(BUT_ADD)

/* Function for enabling the caused by the button being pressed */
void butEnbIrq();

/*! Function for waiting until there is 
 *  a clear button press 
 */
void butPol();


/*! Function for getting the button pressed event filtered.
 */
int butGetCln();
