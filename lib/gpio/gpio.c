// ===========================================================================
//  File          :    gpio.c
//  Entity        :    gpio
//  Purpose       :    General Purpose I/O library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Aug-2012
//  File history    :
// ===========================================================================


char gpioGetBit(char gpio) {
   return gpio & 0xF;
}

char gpioGetReg(char gpio) {
   return (gpio >> 4) & 0xF;
}
