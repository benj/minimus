// ===========================================================================
//  File          :    gpio.h
//  Entity        :    gpio
//  Purpose       :    GPIO library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================



#include "../rot/rot.h"
#include "avr/io.h"

#define GPIO_B_ADD 0x30
#define GPIO_C_ADD 0x60
#define GPIO_D_ADD 0x90

#define GPIO_IN_VAL_OFF  0x0
#define GPIO_DIR_OFF     0x1
#define GPIO_OUT_VAL_OFF 0x2

#define IN  0
#define OUT 1

char gpioGetBit(char gpio);

char gpioGetReg(char gpio);

#define gpioSetDir(gpio, dir) \
   SETBITVAL(_SFR_IO8(gpioGetReg(gpio) + GPIO_DIR_OFF), gpioGetBit(gpio), dir);

#define gpioSetVal(gpio, val) \
   SETBITVAL(_SFR_IO8(gpioGetReg(gpio) + GPIO_OUT_VAL_OFF), gpioGetBit(gpio), val);

#define gpioSet(gpio) \
   SETBIT(_SFR_IO8(gpioGetReg(gpio) + GPIO_OUT_VAL_OFF), gpioGetBit(gpio));

#define gpioClr(gpio) \
   CLRBIT(_SFR_IO8(gpioGetReg(gpio) + GPIO_OUT_VAL_OFF), gpioGetBit(gpio));

#define gpioGetVal(gpio) \
   (_SFR_IO8(gpioGetReg(gpio) + GPIO_IN_VAL_OFF) & (1 << gpioGetBit(gpio)))

#define gpioTgl(gpio) \
   TGLBIT(_SFR_IO8(gpioGetReg(gpio) + GPIO_OUT_VAL_OFF), gpioGetBit(gpio));
