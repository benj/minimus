// ===========================================================================
//  File          :    ssd.c
//  Entity        :    ssd
//  Purpose       :    Source code for controlling the Seven Segment Display.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

#include "ssd.h"
#include "../utl/utl.h"
#include "util/delay.h"

#define SSD_RPL_TME_US 10000 /*!< SSD ripple time in microseconds.*/

/* Function for printing a nibble. */
void inline ssdPrnNbl(char bte);

// Byte to be displayed by the ssd.
char *ssdVal;

/*! Timer 0 Interrupt which, if enabled, updates the SSD with the ssdVal periodically.*/
#ifdef SSD_TMR0_IRQ
#  ifdef TMR0_COMPA_IRQ 
#     warning "ISR for TIMER0_COMPA_vect has been defined multiple times."
#  else
#     define TMR0_COMPA_IRQ
#  endif
#include "../led/led.h"
#include "avr/interrupt.h"
ISR(TIMER0_COMPA_vect) {
   static int dgtSel = 1;

   // Seperate the byte to two nibbles.
   char nbl;

   if (dgtSel) {
      nbl = (*ssdVal >> 4) & 0xF;
      // Print right digit.
      ssdEnbRgtDgt();
   }
   else {
      nbl = *ssdVal & 0xF;
      // Print the left digit.
      ssdEnbLftDgt();
   }
   ssdPrnNbl(nbl);
   dgtSel = !dgtSel;
}
#endif

/* Funvction for clearing the seven segment display */
void inline ssdClr() {
   ssdClrLowLft();
   ssdClrUprLft();
   ssdClrTop();
   ssdClrUprRgt();
   ssdClrLowRgt();
   ssdClrBot();
   ssdClrDot();
   ssdClrMdl();
}

/* Function for printing a byte at the seven segment display board. */
void ssdPrn(char bte)  {
   
   // Seperate the byte to two nibbles.
   char lftNbl = (bte >> 4) & 0xF;
   char rgtNbl = bte & 0xF;

   // Print right digit.
   ssdEnbRgtDgt();
   ssdPrnNbl(rgtNbl);

   _delay_us(SSD_RPL_TME_US);

   // Print the left digit.
   ssdEnbLftDgt();
   ssdPrnNbl(lftNbl);

   _delay_us(SSD_RPL_TME_US);
}


/*! Function for printing a nibble. */
void inline ssdPrnNbl(char bte) {
   ssdClr();
   switch ( bte ) {
      case 0x0 : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetLowRgt();
         ssdSetLowLft();
         ssdSetBot();
         break;
      }
      case 0x1 : {
         ssdSetLowRgt();
         ssdSetUprRgt();
         break;
      }
      case 0x2 : {
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetMdl();
         ssdSetLowLft();
         ssdSetBot();
         break;
      }
      case 0x3 : {
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetMdl();
         ssdSetLowRgt();
         ssdSetBot();
         break;
      }
      case 0x4 : {
         ssdSetUprLft();
         ssdSetUprRgt();
         ssdSetMdl();
         ssdSetLowRgt();
         break;
      }
      case 0x5 : {
         ssdSetTop();
         ssdSetUprLft();
         ssdSetMdl();
         ssdSetLowRgt();
         ssdSetBot();
         break;
      }
      case 0x6 : {
         ssdSetTop();
         ssdSetUprLft();
         ssdSetLowRgt();
         ssdSetBot();
         ssdSetLowLft();
         ssdSetMdl();
         break;
      }
      case 0x7 : {
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetLowRgt();
         break;
      }
      case 0x8 : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetLowRgt();
         ssdSetBot();
         ssdSetMdl();
         break;
      }
      case 0x9 : {
         ssdSetUprLft();
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetLowRgt();
         ssdSetMdl();
         break;
      }
      case 0xA : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetUprRgt();
         ssdSetLowRgt();
         ssdSetMdl();
         break;
      }
      case 0xB : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetLowRgt();
         ssdSetBot();
         ssdSetMdl();
         break;
      }
      case 0xC : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetBot();
         break;
      }
      case 0xD : {
         ssdSetLowLft();
         ssdSetUprRgt();
         ssdSetLowRgt();
         ssdSetBot();
         ssdSetMdl();
         break;
      }
      case 0xE : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetBot();
         ssdSetMdl();
         break;
      }
      case 0xF : {
         ssdSetLowLft();
         ssdSetUprLft();
         ssdSetTop();
         ssdSetMdl();
         break;
      }
      default : {
         // Error message for value out of range is the dot.(!)
         ssdSetDot();
      }
   }
}
