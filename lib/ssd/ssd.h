// ===========================================================================
//  File          :    ssd.h
//  Entity        :    ssd
//  Purpose       :    Seven Segment Display library module header.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

#include "../gpio/gpio.h"

// GPIO addresses of the SSD device.
#define SSD_PRT_1  GPIO_B_ADD + 6
#define SSD_PRT_2  GPIO_C_ADD + 2
#define SSD_PRT_3  GPIO_D_ADD + 0
#define SSD_PRT_4  GPIO_D_ADD + 6
#define SSD_PRT_5  GPIO_D_ADD + 4
#define SSD_PRT_6  GPIO_C_ADD + 8
#define SSD_PRT_7  GPIO_C_ADD + 5
#define SSD_PRT_8  GPIO_C_ADD + 6
#define SSD_PRT_9  GPIO_C_ADD + 7
#define SSD_PRT_10 GPIO_B_ADD + 7

#define SSD_MDL_ADD     SSD_PRT_1
#define SSD_DOT_ADD     SSD_PRT_2
#define SSD_TOP_ADD     SSD_PRT_3
#define SSD_UPR_LFT_ADD SSD_PRT_4
#define SSD_NSL_RGT_ADD SSD_PRT_5
#define SSD_BOT_ADD     SSD_PRT_6
#define SSD_LOW_LFT_ADD SSD_PRT_7
#define SSD_LOW_RGT_ADD SSD_PRT_8
#define SSD_UPR_RGT_ADD SSD_PRT_9
#define SSD_NSL_LFT_ADD SSD_PRT_10

/* Macros for setting the ports */
#define ssdSetLowLft() gpioSet(SSD_LOW_LFT_ADD);
#define ssdSetUprLft() gpioSet(SSD_UPR_LFT_ADD);
#define ssdSetTop()    gpioSet(SSD_TOP_ADD);
#define ssdSetUprRgt() gpioSet(SSD_UPR_RGT_ADD);
#define ssdSetLowRgt() gpioSet(SSD_LOW_RGT_ADD);
#define ssdSetBot()    gpioSet(SSD_BOT_ADD);
#define ssdSetDot()    gpioSet(SSD_DOT_ADD);
#define ssdSetMdl()    gpioSet(SSD_MDL_ADD);

/* Macros for clearing the ports */
#define ssdClrLowLft() gpioClr(SSD_LOW_LFT_ADD);
#define ssdClrUprLft() gpioClr(SSD_UPR_LFT_ADD);
#define ssdClrTop()    gpioClr(SSD_TOP_ADD);
#define ssdClrUprRgt() gpioClr(SSD_UPR_RGT_ADD);
#define ssdClrLowRgt() gpioClr(SSD_LOW_RGT_ADD);
#define ssdClrBot()    gpioClr(SSD_BOT_ADD);
#define ssdClrDot()    gpioClr(SSD_DOT_ADD);
#define ssdClrMdl()    gpioClr(SSD_MDL_ADD);

// Macros for enabling/disabling the digits of a seven segement display.
#define ssdSetNslRgt() gpioSet(SSD_NSL_RGT_ADD);
#define ssdSetNslLft() gpioSet(SSD_NSL_LFT_ADD);
#define ssdClrNslRgt() gpioClr(SSD_NSL_RGT_ADD);
#define ssdClrNslLft() gpioClr(SSD_NSL_LFT_ADD);

/* Macro for enabling each digit */
#define ssdEnbRgtDgt()\
   ssdClrNslRgt();\
   ssdSetNslLft();

/* Macro for enabling each digit */
#define ssdEnbLftDgt()\
   ssdClrNslLft();\
   ssdSetNslRgt();

/*! Macro for initializing the I/O ports */
#define ssdIni() \
   gpioSetDir(SSD_LOW_LFT_ADD, OUT); \
   gpioSetDir(SSD_UPR_LFT_ADD, OUT); \
   gpioSetDir(SSD_TOP_ADD, OUT); \
   gpioSetDir(SSD_UPR_RGT_ADD, OUT); \
   gpioSetDir(SSD_LOW_RGT_ADD, OUT); \
   gpioSetDir(SSD_BOT_ADD, OUT); \
   gpioSetDir(SSD_DOT_ADD, OUT); \
   gpioSetDir(SSD_MDL_ADD, OUT); \
   gpioSetDir(SSD_NSL_RGT_ADD, OUT); \
   gpioSetDir(SSD_NSL_LFT_ADD, OUT);

/* Macro for clearing all the leds */

/*! Funvction for clearing the seven segment display */
void inline ssdClr();

/*! Function for printing a byte at the seven segment display board. */
void ssdPrn(char bte);

/*! Function for printing a nibble. */
void inline ssdPrnNbl(char bte);
