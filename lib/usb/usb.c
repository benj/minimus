// ===========================================================================
//  File          :    usbAvr.c
//  Entity        :    usbAvr
//  Purpose       :    USB 2.0 AVR ATmega32u2 Library Module Source Code.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 07-Aug-2012
//  File history    :
// ===========================================================================

//#include "usbPrt.h"
#include "usb.h"
#include "avr/io.h"
#include "../lib.h"
#include "avr/interrupt.h"
#include "util/delay.h"

#ifdef USB_IRQ
ISR(USB_GEN_vect) {
   ledIni(LED_RED);
   ledSet(LED_RED);
   return 0;
}
#endif

/*!A function for resetting the USB Interface.*/
void usbRst()  {
   char tmp = USBCON;
   USBCON = (tmp & ~(1 << USBE));
   USBCON = (tmp |  (1 << USBE));
}

/*! Function for turning off the USB module.*/
void usbPowOff() {
   wdtDsb();
   usbDtc();
   usbIrqDsb();
   usbDsb();
   clkDsbPll();
   usbIrqClr();
}

/*! Function for the initialization of the USB.*/
void usbPowOn() {
   clkSetUsb();
	usbRst();
   usbEnbClk();
   usbAtt();
   usbEnb();
   usbIrqEnb();

   // Set the control endpoint.
   usbSetEnpNum(0); 
   usbEnbEnp();
   UECFG1X = 0;
   usbSetEnpTrfTpe(USB_ENP_CTL);
   usbSetEnpSze(USB_ENP_SZE_64);
   usbSetEnpBnkNum(USB_ENP_BNK_2);
   usbAlcMem();
   
   char i = 0;
   while (usbChkMem() != 1 && i != (char)255) {
      UECFG1X = i;
      usbAlcMem();
      i++;
   }
   sei();
}
