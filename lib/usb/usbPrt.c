// ===========================================================================
//  File          :    usb.c
//  Entity        :    usb
//  Purpose       :    USB 2.0 Library Module Source Code.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 07-Aug-2012
//  File history    :
// ===========================================================================

#include "usbPrt.h"

usb_t usb; /*!< Structure representing the USB module.*/

/*! USB Control Transfer Data OUT function.*/
void usbStpPckOut();

/*! USB Control Transfer Data IN  function.*/
void usbStpPckInp();

/*! USB Control Transfer Status Stage OUT function.*/
void usbOutPckZLP();

/*! Function for holding the acknowledgment from the host.*/
void usbAck();

/*! USB Control Transfer SETUP Function.*/
void usbStpPck(char enpIdx) {
   // Check for error.
   if (usb.fun.getErrFrm())
      return;

   // Fill the usb control transfer structure.*/
   usb.fun.read(usb.ctr, sizeof(usbCtr_t));

   switch ( getReqTpe(usb.ctr.tpe) ) {
      case USB_REQ_TPE_STD : {
         switch ( usb.ctr.req ) {
            case USB_CLR_FEA_REQ : {
               switch ( getReqRec(usb.ctr.tpe) ) {
                  case USB_REQ_REC_DEV : {
                     switch ( usb.ctr.val ) {
                        case DEVICE_REMOTE_WAKEUP : {
                           usb.dev.rmoWupEnb = USB_FET_DSB;
                           break;
                        }
                        case TEST_MODE : {
                           usb.dev.tstMdeEnb = USB_FET_DSB;
                           break;
                        }
                     }
                  }
                  case USB_REQ_REC_INF : {
                     // Nothing to be implemented here.
                     break;
                  }
                  case USB_REQ_REC_ENP : {
                     switch ( usb.ctr.req.val ) {
                        case ENDPOINT_HALT : {
                           usb.dev.cfg->inf->enp[enpIdx]->hlt = USB_FET_DSB;
                           break;
                        }
                     } 
                  }
               }
            }
            case USB_SET_FEA_REQ : {
               switch ( getReqRec(usb.ctr.tpe) ) {
                  case USB_REQ_REC_DEV : {
                     switch ( usb.ctr.val ) {
                        case DEVICE_REMOTE_WAKEUP : {
                           usb.dev.rmoWupEnb = USB_FET_ENB;
                           break;
                        }
                        case TEST_MODE : {
                           usb.dev.tstMdeEnb = USB_FET_ENB;
                           break;
                        }
                     }
                  }
                  case USB_REQ_REC_INF : {
                     // Nothing to be implemented here.
                     usb.fun.ackStp();
                     break;
                  }
                  case USB_REQ_REC_ENP : {
                     switch ( usb.ctr.req.val ) {
                        case ENDPOINT_HALT : {
                           usb.dev.cfg->inf->enp[enpIdx]->hlt = USB_FET_ENB;
                           break;
                        }
                     } 
                  }
               }
            }
            case USB_SET_ADD_REQ : {
               usb.fun.setAdd(usb.ctr.val);
               usb.fun.in();
               usb.fun.valAdd();
               break;
            }
            case USB_SET_CFG_REQ : {
               usb.dev.enb = usb.fun.popWrd();
               break;
            }
            default : {
               // Set the function pointers.
               usb.getInpPck = &usbStpInpPck();
               usb.getOutPck = &usbStpOutPck();
               usb.fun.ack();
               return;
            }
         }
      }
      case USB_REQ_TPE_CLS : {
         //?? Not Implemented.
      }
      case USB_REQ_TPE_VND : {
         //?? Not Implemented.
      }
   }

   usb.fun.ack();
   return;

}

/*! USB  Input Packet during SETUP process.*/
void usbStpPckInp(char enpIdx) {
   // Check for error.
   if (usb.fun.getErrFrm())
      return;

   switch ( getReqTpe(usb.ctr.tpe) ) {
      case USB_REQ_TPE_STD : {
         switch ( usb.ctr.req ) {
            case USB_GET_STA_REQ : {
               switch ( getReqRec(usb.ctr.tpe) ) {
                  case USB_REQ_REC_DEV : {
                     usb.fun.pshWrd(usb.dev.sta);
                     break;
                  }
                  case USB_REQ_REC_INF : {
                     usb.fun.pshWrd(usb.dev.cfg.inf.sta);
                     break;
                  }
                  case USB_REQ_REC_ENP : {
                     usb.fun.pshWrd(usb.dev.cfg->inf->enp[enpIdx]->sta);
                     break;
                  }
               }
            }
            case USB_GET_DSC_REQ : {
               switch ( getReqDscTpe(usb.ctr.req) ) {
                  case : USB_DEV_DSC_TPE : {
                     usb.fun.write(usb.dev, usb.dev.len);
                     break;
                  }
                  case : USB_CFG_DSC_TPE : {
                     usb.fun.write(&usb.dev->cfg, usb.dev.cfg->len);
                     break;
                  }
                  case : USB_INF_DSC_TPE : {
                     usb.fun.write(&usb.dev.cfg->inf, usb.dev.cfg->inf->len);
                     break;
                  }
                  case : USB_ENP_DSC_TPE : {
                     usb.fun.write(&usb.dev.cfg->inf->enp[enpIdx], usb.dev.cfg->inf->enp[enpIdx]->len);
                     break;
                  }
               }
            }
            case USB_GET_CFG_REQ : {
               usb.fun.pshWrd(usb.enb);
               break;
            }
         }
      }
      case USB_REQ_TPE_CLS : {
         //?? Not Implemented.
         break;
      }
      case USB_REQ_TPE_VND : {
         //?? Not Implemented.
         break;
      }
      default : return;
   }

   usb.fun.in();
   usb.outPck = &usbOutPckZLP();
   usb.fsm    = USB_WAI_ACK;
}

/*! USB Control Transfer Status Stage OUT function.*/
void usbOutPckZLP() {
   if      (usb.fsm == USB_REC_ACK) {
      usb.fun.ack();
   }
   else if (usb.fsm == USB_MOD_PRC) {
      // usb.fun.nak(); Not implemented yet for AVR.
   } 
   else {
      usb.fun.stl();
   }
}

/*! USB  Output Packet during SETUP process.*/
void usbStpPckOut(char enpIdx) {
   // Check for error.
   if (usb.fun.getErrFrm())
      return;

   switch ( getReqTpe(usb.ctr.tpe) ) {
      case USB_REQ_TPE_STD : {
         switch ( usb.ctr.req ) {
            case USB_SET_DSC_REQ : {
               switch ( getReqDscTpe(usb.ctr.req) ) {
                  case : USB_DEV_DSC_TPE : {
                     usb.fun.read(usb.dev, usb.dev.len);
                     break;
                  }
                  case : USB_CFG_DSC_TPE : {
                     usb.fun.read(&usb.dev->cfg, usb.dev.cfg->len);
                     break;
                  }
                  case : USB_INF_DSC_TPE : {
                     usb.fun.read(&usb.dev.cfg->inf, usb.dev.cfg->inf->len);
                     break;
                  }
                  case : USB_ENP_DSC_TPE : {
                     usb.fun.read(&usb.dev.cfg->inf->enp[enpIdx], usb.dev.cfg->inf->enp[enpIdx]->len);
                     break;
                  }
               }
            }
         }
      }
      case USB_REQ_TPE_CLS : {
         //?? Not Implemented.
         usb.fun.ack();
         break;
      }
      case USB_REQ_TPE_VND : {
         //?? Not Implemented.
         usb.fun.ack();
         break;
      }
      default : {
         return;
      }
   }

   usb.fun.ack();
   usb.inpPck = &usbOutPckZLP();
   return;
}

/*! Function for acknowledging by sending ZLP.*/
void usbOutPckZLP() {
   usb.fun.in();
   usb.fsm = USB_WAI_ACK;
}


/*! Function for seding an interrupt.*/
void usbInpIrq(char *buf. size_t cnt) {
   usb.fun.write(buf, cnt);
   usb.inpPck = &usbOutPckZLP;
}

/*! Function for receiving an interrupt OUT packet.*/
void usbOutIrq(char *buf, size_t cnt) {
   usb.fun.read(buf, cnt);
   usb.fun.ack();
}

/*! Isochronous IN Transfer.*/
void usbIsoPckInp(char *buf, size_t cnt) {
   usb.fun.write(buf, cnt);
   usb.fun.in();
}

/*! Isochronous OUT Transfer.*/
void usbIsoPckOut(char *buf, size_t cnt) {
   usb.fun.read(buf, cnt);
}

/*! Function for holding the acknowledgment from the host.*/
void usbAck() {
   usb.fsm = USB_REC_ACK;
}
