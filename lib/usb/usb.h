// ===========================================================================
//  File          :    usbAvr.h
//  Entity        :    usbAvr
//  Purpose       :    USB 2.0 AVR ATMega32u2 Specific Library Module.
//  Documentation :   
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 07-Aug-2012
//  File history    :
// ===========================================================================
#include "avr/io.h"
#include "../rot/rot.h"

/* Macro for enabling the USB*/
#define usbEnb() SETBIT(USBCON, USBE)

/* Macro for disabling the USB*/
#define usbDsb() CLRBIT(USBCON, USBE)

/*! Macro for disabling the USB interrupts.*/
#define usbIrqDsb() UDIEN = 0

/*! Macro for enabling the USB interrupts.*/
#define usbIrqEnb() UDIEN |= 0b01111101;

/*! Macro for clearing the USB interrupts.*/
#define usbIrqClr() UDINT = 0

/* Macro for enabling the USB clock. */
#define usbEnbClk() CLRBIT(USBCON, FRZCLK);

/* Macro for freezing the USB clok. */
#define usbDsbClk() SETBIT(USBCON, FRZCLK);

/* Macro for enabling the direct drive of the USB Buffers. */
#define usbEnbBufDrv() \
   SETBIT(UPOE, UPWE1);\
   CLRBIT(UPOE, UPWE0);

/* Macro for disabling the direct drive of the USB Buffers. */
#define usbDsbBufDrv() \
   CLRBIT(UPOE, UPWE1);\
   CLRBIT(UPOE, UPWE0);

/* Set USB Data pins. */
#define usbSetDatPin(dpnPls, dpnMns) \
   SETBITVAL(UPOE, UPDRV1, dpnPls);\
   SETBITVAL(UPOE, UPDRV0, dpnMns);

/* Macro for reading the D+ pin data.*/
#define usbGetDatPlsPin() GETBITVAL(UPOE, DPI)

/* Macro for reading the D- pin data.*/
#define usbGetDatMnsPin() GETBITVAL(UPOE, DMI)

/* Macro for enabling the 3.3V regulator.*/
#define usbEnbRgl() SETBIT(REGCR, REGDIS)

/* Macro for disabling the 3.3V regulator.*/
#define usbDisRgl() CLRBIT(REGCR, REGDIS)

/* Macro for latched reset the CPU, but the USB module.*/
#define usbRstLatCpu() SETBIT(UDCON, RSTCPU)

/* Macro for recovering the CPU, after reset.*/
#define usbRstRecCpu() CLRBIT(UDCON, RSTCPU)

/* Macro for generating an remote wake-up packet.*/
#define usbSetRmoWkeUp() SETBIT(UDCON, RMWKUP)

/* Macro for detaching the USB module from the bus.*/
#define usbDtc() SETBIT(UDCON, DETACH)

/* Macro for attaching the USB module to the bus.*/
#define usbAtt() CLRBIT(UDCON, DETACH)

/*! Macro for reading the electrical status of the USB (attached/detached).*/
#define usbGetDtc() GETBITVAL(UDCON, DETACH)

/* Function for getting the USB Interrupt type.*/
#define usbIrqInv 0xFF /*!< Definition for invalid interrupt type.?? Consider removal.*/

/* Macro for enabling the upstream resume interrupt.*/
#define usbEnbUpsRsmIrq() SETBIT(UDIEN, UPRSME)

/* Macro for enabling the end of resume interrupt.*/
#define usbEnbEORIrq() SETBIT(UDIEN, EORSME)

/* Macro for enabling the wake-up CPU interrupt.*/
#define usbEnbWakCpuIrq() SETBIT(UDIEN, WAKEUPE)

/* Macro for enabling the Start of Frame Interrupt.*/
#define usbEnbSofIrq() SETBIT(UDIEN, SOFE)

/* Macro for enabling the suspend interrupt.*/
#define usbEnbSusInt() SETBIT(UDIEN, SUSPE)

/* Macro for disabing the upstream resume interrupt.*/
#define usbDsbUpsRsmIrq() CLRBIT(UDIEN, UPRSME)

/* Macro for disabing the end of resume interrupt.*/
#define usbDsbEORIrq() CLRBIT(UDIEN, EORSME)

/* Macro for disabing the wake-up CPU interrupt.*/
#define usbDsbWakCpuIrq() CLRBIT(UDIEN, WAKEUPE)

/* Macro for disabing the Start of Frame Interrupt.*/
#define usbDsbSofIrq() CLRBIT(UDIEN, SOFE)

/* Macro for disabing the suspend interrupt.*/
#define usbDsbSusIrq() CLRBIT(UDIEN, SUSPE)

/* Macro for validating the USB address register content.*/
#define usbAddVld() SETBIT(UDADDR, ADDEN)

/* Macro for invalidating the USB address register content.*/
#define usbInvAdd() CLRBIT(UDADDR, ADDEN)

/* Macro for setting the address of the USB device.*/
#define usbSetAdd(add) (UDADDR = add)

/* Macro for validating  the address of the USB device.*/
#define usbValAdd() (UDADDR = (1 << ADDEN))

/* Macro for getting the USB Device Frame Number.*/
#define usbGetFnm() (((UDFNUMH & 0x3) << 8) | UDFNUML)

/* Macro that checks for corrupted frame number is a start of frame packet.*/
#define usbChkFrmNumErr() GETBITVAL(FNCERR)

/* Macro that checks whether the USB module is in suspend state.*/
#define usbChkSusStt() GETBITVAL(UDINT, SUSPI)

/* Macro for setting the endpoint number.*/
#define usbSetEnpNum(num) UENUM |= (num &  0x7)

/* Macro for latched resetting the endpoint.*/
#define usbRstLatEnp(enp) SETBIT(UERST, enp)

/* Macro for reconvering from endpoint reset.*/
#define usbRstRecEnp(enp) CLRBIT(UERST, enp)

/* Macro for generating a STALL answer.*/
#define usbGenStl() SETBIT(UECONX, STALLRQ)

/* Macro for disabling pending stall handshake.*/
#define usbClrStl() SETBIT(UECONX, STALLRQC)

/* Macro for resetting the data toggle for the selected endpoint.*/
#define usbRstDatTgl() SETBIT(UECONX, RSTDT)

/* Macro for enabling the selected endpoint.*/
#define usbEnbEnp() SETBIT(UECONX, EPEN)

/* Macro for disabling endpoint.*/
#define usbDsbEnp() CLRBIT(UECONX, EPEN)

/* Macro for defining the endpoint transfer type.*/
#define USB_ENP_CTL 0
#define USB_ENP_ISO 1
#define USB_ENP_BLK 2
#define USB_ENP_INT 3
#define usbSetEnpTrfTpe(tpe) (UECFG0X |= ((tpe & 0x3) << EPTYPE0))

/* Macro for setting the endpoint direction.*/
#define USB_ENP_OUT 0
#define USB_ENP_IN  1
#define usbSetEnpDir(dir) SETBITVAL(UECFG0X, EPDIR, dir)

/* Macro for setting the endpoint size.*/
#define USB_ENP_SZE_8  0
#define USB_ENP_SZE_16 1
#define USB_ENP_SZE_32 2
#define USB_ENP_SZE_64 3
#define usbSetEnpSze(sze) (UECFG1X |= ((sze & 0x3) << 4))

/* Macro for setting the endpoint number of banks.*/
#define USB_ENP_BNK_1 0
#define USB_ENP_BNK_2 1
#define usbSetEnpBnkNum(num) (UECFG1X |= ((num & 0x3) << EPBK0))

/* Macro for allocating the specified memory as endpointSize x number of banks.*/
#define usbAlcMem() SETBIT(UECFG1X, ALLOC)

/* Macro for checking valid memory allocation.*/
#define usbChkMem() GETBITVAL(UESTA0X, CFGOK)

/* Macro for checking for overflow in isonchronous endpoint.*/
#define usbGetOvf() (GETBITVAL(UESTA0X,  OVERFI) & GETBITVAL(UEIENX, FLERRE))

/* Macro for checking for underflow in isonchronous endpoint.*/
#define usbGetUnf() (GETBITVAL(UESTA0X, UNDERFI) & GETBITVAL(UEIENX, FLERRE))

/* Macro for PID data indication.*/
#define USB_PID_DAT_0 0
#define USB_PID_DAT_1 1
#define usbGetPid() GETBITVAL(UESTA0X, DTSEQ1)

/* Macro for Busy Banks indication.*/
#define USB_BNK_FRE 0 /*!< Definition of all banks being free.*/
#define USB_BNK_1BS 1 /*!< Definition of one bank  being busy,*/
#define USB_BNK_ALL 2 /*!< Definition of two banks being busy*/
#define usbGetBsyBnk() (UESTA0X & 0x3)

/* Macro for getting the direction of the following packet.*/
#define usbGetPckDir() GETBITVAL(UESTA1X, CTRLDIR)

/* Macro for getting the bank currently being used.*/
#define USB_BNK_USD_1 0
#define USB_BNK_USD_2 1
#define usbGetUsdBnk() GETBITVAL(UESTA1X, CURRBK0)

#define usbEnpEnbIrq(irq) SETBIT(irq, UEIENX)
/*! Macro for disabling endpoint interrupt. */
#define usbEnpDsbIrq(irq) CLRBIT(irq, UEIENX)

/*! Macro for reading from the current bank.*/
#define usbBnkPsh(bte) UEDATX = bte

/*! Macro for raeding from the current bank.*/
#define usbBnkPop() (UEDATX + 0)

/*! Macro for reading the number of valid bytes in the current bank.*/
#define usbGetBnkValBte() (UEBCLX + 0)

/*! Definitions of the endpoints.*/
#define USB_ENP_0 0
#define USB_ENP_1 1
#define USB_ENP_2 2
#define USB_ENP_3 3
#define USB_ENP_4 4

/*! Macro for acknowledging a SETUP packet received.*/
#define usbStpAck() CLRBIT(UEINTX, RXSTPI)

/*! Macro for acknowleding an IN packet.*/
#define usbInpAck() CLRBIT(UEINTX, RXOUTI)

/*! Macro for generating an IN packet.*/
#define usbGenInp() CLRBIT(UEINTX, TXINI)

/*! Function for powering on the USB module.*/
void usbPowOn();

/*! Function for powering off the USB module.*/
void usbPowOff();

/*! Function for reset of the USB module.*/
void usbRst();
