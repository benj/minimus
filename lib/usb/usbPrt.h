// ===========================================================================
//  File          :    usb.h
//  Entity        :    usb
//  Purpose       :    USB 2.0 Library Module Header.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 07-Aug-2012
//  File history    :
// ===========================================================================
#include "avr/io.h"

// The Setup Packet: Each request starts with an 8 byte 
// long Setup Packet which has the following format.
// bmRequestType
// bRequest
// wValue
// wIndex
// wLength
//// bmRequestPacket fields
////// Data Phase Transfer Direction.
#define USB_REQ_DIR_BIT 7        /*!< Data Phase Transfer Direction bit position*/
#define USB_REQ_DIR_MSK 0x1      /*!< Data Phase Transfer Direction bits' mask*/
#define USB_REQ_DIR_HST_TO_DEV 0 /*!< Data Phase Transfer Direction Host to Device.*/
#define USB_REQ_DIR_DEV_TO_HST 1 /*!< Data Phase Transfer Direction Device to Host.*/
////// Type
#define USB_REQ_TPE_BIT 5   /*!< Type bit position.*/
#define USB_REQ_TPE_MSK 0x3 /*!< Type bits' mask.*/
#define USB_REQ_TPE_STD 0   /*!< Type Standard.*/
#define USB_REQ_TPE_CLS 1   /*!< Type Class.*/
#define USB_REQ_TPE_VND 2   /*!< Type Vendor.*/
////// Recipient
#define USB_REQ_REC_BIT 0    /*!< Recipient bit position*/
#define USB_REQ_REC_MSK 0x1F /*!< Recipient bits' mask*/
#define USB_REQ_REC_DEV 0    /*!< Recipient device.*/
#define USB_REQ_REC_INF 1    /*!< Recipient interface.*/
#define USB_REQ_REC_ENP 2    /*!< Recipient endpoint.*/
#define USB_REQ_REC_OTH 3    /*!< Recipient other.*/

//// Generic requests.
#define USB_GET_STA_REQ 0x0               /*!< bRequest value for Get Status.*/
#define USB_CLR_FEA_REQ 0x1               /*!< bRequest value for Clear Feature.*/
#define USB_SET_FEA_REQ 0x3               /*!< bRequest value for Set Feature.*/
#define USB_SET_ADD_REQ 0x5               /*!< bRequest value for Set Address.*/
#define USB_GET_DSC_REQ 0x6               /*!< bRequest value for Get Descriptor.*/
#define USB_SET_DSC_REQ 0x7               /*!< bRequest value for Set Descriptor.*/
#define USB_GET_CFG_REQ 0x8               /*!< bRequest value for Get Configuration.*/
#define USB_SET_CFG_REQ 0x9               /*!< bRequest value for Set Configuration.*/
#define USB_STD_GET_STA_REQ_SLF_POW_BIT 0 /*!< Request Data Bit for Self Powered.*/
#define USB_STD_GET_STA_REQ_REM_WUP_BIT 1 /*!< Request Data Bit for Remote Wakeup.*/
//// Descriptor Values
#define USB_DEV_DSC_TPE 0x01
#define USB_CFG_DSC_TPE 0x02
#define USB_STR_DSC_TPE 0x03
#define USB_INF_DSC_TPE 0x04
#define USB_ENP_DSC_TPE 0x05

//// Interface requests.
#define USB_GET_INF_REQ 0x0A /*!< bRequest value for Get Interface.*/
#define USB_SET_INF_REQ 0x11 /*!< bRequest value for Set Interface.*/

//// Endpoint requests.
#define USB_SNC_FRM_REQ 0x12 /*!< bRequest value for Synch Frame.*/

// Configuration values
#define USB_FET_ENB 1
#define USB_FET_DSB 0
#define getUsbCfgAtr(slfPow, remWup)  0x80 + (slfPow << 6) + (remWup << 5)

// Endpoint values.
#define USB_ENP_HLT 1
#define USB_ENP_STL 0

/*!
 * \struct String Structure
 */
struct usbStrStr {
   char len;                   /*!< Size in Bytes.*/
   char tpe = USB_STR_DSC_TPE; /*!< Descriptor.*/
   int *str;                   /*!< Unicode Encoded String.*/
};

typedef struct usbStrStr usbStr_t;

/*!
 * \struct String Endpoint Description Structure.
 */
struct usbLngStr {
   int  len;                   /*!< Size of Descriptor in Bytes.*/
   char tpe = USB_STR_DSC_TPE; /*!< Descriptor.*/
   int  *lng;                  /*!< Supported Languages*/
};

typedef usbLngStr usbLng_t;

/*!
 *\struct Endpoint Adress Structure.
 */
struct usbEnpAdrStr
   char num; /*!< Endpoint Number.*/
   char dir; /*!< Direction. Ignored for Control Endpoints.*/
};

typedef struct usbEnpAdrStr usbEnpAdr_t;


/*!
 * \struct Endpoint attrubutes structure.
 */
struct usbEnpAtrStr {
   char trfTpe; /*!< Transfer Type.*/
   // The following fields refer only
   // to isonchronous endpoint.
   char sncTpe; /*!< Synchronization Type.*/
   char useTpe; /*!< Usage Type.*/
};

typedef struct usbEnpAtrStr usbEnpAtr_t;

/*!
 * \struct Endpoint Description Structure.
 */
struct usbEnpStr {
   char        len = 0x7;             /*!< Size of Descriptor in Bytes (7 bytes).*/
   char        tpe = USB_ENP_DSC_TPE; /*!< Descriptor.*/
   usbEnpAdr_t adr;                   /*!< Address.*/
   usbEnpAtr_t atr;                   /*!< Attributes. */
   int         maxPckSze;             /*!< Max Packet Size.*/
   char        irv;                   /*!< Interval for polling endpoint data transfers.
                                           Value in frame counts. Ingored for Bunk & 
                                           Control Endpoints. Isonchronous must equal 1 and
                                           field may range from 1 to 255 for interrupt
                                           endpoints.*/
   int         sta;                   /*!< Endpoint status indicating halted/stalled interface.*/
   struct usbEnpStr *nxt              /*!< Next endpoint in the linked list.*/
};

typedef struct usbEnpStr usbEnp_t;

/*!
 * \struct Interface Descriptors.
 */
struct usbInfStr {
   char             len = 0x9;             /*!< Size of Descriptor in Bytes (9 bytes).*/
   char             tpe = USB_INF_DSC_TPE; /*!< Descriptor.*/
   char             num;                   /*!< Number of Interface.*/
   char             altSet;                /*!< Value used to select alternative setting.*/
   char             enpNum;                /*!< Number of endpoints used for this interface.*/
   char             cls;                   /*!< Class code (Assigned by USB Org).*/
   char             subCls;                /*!< Subclass code (Assigned by USB Org).*/
   char             prt;                   /*!< Protocol code (Assigned by USB Org).*/
   char             infIdx;                /*!< Index of String Descriptor for this interrface.*/
   int              sta;                   /*!< USB Module status indicating Self-Powered, and Remote Wakeup.*/
   usbEnp_t         *enp;                  /*!< Root of endpoints linked list.*/
   struct usbInfStr *nxt;                  /*!< Next Interface in the linked list.*/
};

typedef struct usbInfStr usbInf_t;

/*!
 * \struct Configuration Descriptors.
 */
struct usbCfgStr {
   char             len;                   /*!< Size in Bytes.*/
   char             tpe = USB_CFG_DSC_TPE; /*!< Descriptor,*/
   int              totLen;                /*!< Total length in bytes returned.*/
   char             infNum;                /*!< Number of interfaces.*/
   char             cfgVal;                /*!< Value to used as an argument to use this configuration.*/
   char             cfgIdx;                /*!< Index of String Descriptor describing this configuration.*/
   usbCfgAtr_t      atr;                   /*!< Attributes of this configuration.*/
   char             maxPow;                /*!< Maximum Power Consumption in 2mA units.*/
   usbInf_t         *inf;                  /*!< Root of interface linked list.*/
   struct usbCfgStr *nxt;                  /*!< Next configuration.*/
};

typedef struct usbCfgStr usbCfg_t;

/*!
 * \struct Device Descriptor Structure.
 */
struct usbDevStr {
   char     len = 0x12;            /*!< Size of Descriptor in Bytes.*/
   char     tpe = USB_DEV_DSC_TPE; /*!< Descriptor (0x01).*/
   int      bcdUSB;                /*!< USB Specification Number which device complies with.*/
   char     cls;                   /*!< Class Code (Assigned by USB Org).
                                        If equal to 0x00, each interface specifies it's own class code.
                                        If equal to 0xFF, the class code is vendor specified.
                                        Otherwise field is valid Class Code.
                                    */
   char     subCls;                /*!< Subclass Code (Assigned by USB Org)*/
   char     prt;                   /*!< Protocol Code (Assigned by USB Org)*/
   char     maxPckSze;             /*!< Maximum Packet Size for Zero Endpoint. Valid sizes are 8, 16, 32, 64.*/
   int      vnd;                   /*!< Vendor  ID (Assigned by USB Org).*/
   int      prd;                   /*!< Product ID (Assigned by USB Org).*/
   int      bcdDev;                /*!< Device Release Number.*/
   char     mnfStr;                /*!< Index  of Manufacturer  String Descriptor.*/
   char     prdStr;                /*!< Index  of Product       String Descriptor.*/
   char     srlStr;                /*!< Index  of Serial Number String Descriptor.*/
   char     cfgNum;                /*!< Number of Possible configurations.*/
   char     sta;                   /*!< USB Module status indicating Self-Powered, and Remote Wakeup.*/
   char     rmoWupEnb;             /*!< USB Remote wake-up feature enabled.*/
   char     tstMdeEnb;             /*!< USB Test mode feature enabled.*/
   usbCfg_t *cfg;                  /*!< Configuration descriptor.*/
};

typedef struct usbDevStr usbDev_t;

#define getReqRec(req)    ((req >> USB_REQ_REC_BIT) & USB_REQ_REC_MSK)
#define getReqTpe(req)    ((req >> USB_REQ_TPE_BIT) & USB_REQ_TPE_MSK)
#define getReqDir(req)    ((req >> USB_REQ_DIR_BIT) & USB_REQ_DIR_MSK)

/*
 * \struct Control. Structure for control transfers related data.
 */
struct usbCtrStr {
   char tpe; /*!< bmRequestType. Direction-Type-Recepient.*/
   char req; /*!< bRequest.      Request.*/
   int  val; /*!< wValue.        Value of the request.*/
   int  idx; /*!< wIndex.        Endpoint selection.*/
   int  len; /*!< wLength.       Number of bytes to transfer.*/
};

typedef usbCtrStr usbCtr_t;

#define getReqDscTpe(req) (req.val & 0xFF) //??Set without reference to the standard.*/
#define getReqDscIdx(req) (req.val >> 8)   //??Set without reference to the standard.*/

/*!
 * \struct Functions. USB Functions supported from the CPU.
 */
struct usbFunStr {
   void   (*read)  (void *buf, size_t cntBte); /*!< USB Function for reading from a fixed location to   the buffer. */
   void   (*write) (void *buf, size_t cntBte); /*!< USB Function for writing to   a fixed location from the buffer. */
   char   (*popBte)();                         /*!< USB Function for popping a byte from a fixed location. */
   void   (*pshBte)(char);                     /*!< USB Function for pushing a byte from a fixed location. */
   int    (*popWrd)();                         /*!< USB Function for popping a word from a fixed location. */
   void   (*pshWrd)(int);                      /*!< USB Function for pushing a word from a fixed location. */
   void   (*setAdd)(char);                     /*!< USB Function for setting the address. */
   void   (*valAdd)();                         /*!< USB Function for validating the address. */
   void   (*ack)();                            /*!< USB Function for acknowledging  */
   void   (*nak)();                            /*!< USB Function for not-ready response. */
   void   (*stl)();                            /*!< USB Function for operation not supported/endpoint halted response. */
   void   (*in)();                             /*!< USB Function for IN  */
   char   (*getErrFrm)();                      /*!< USB Function for validating whether the frame was erroneous. */
};

typedef struct usbFunStr usbFun_t;

#define USB_WAI_ACK
#define USB_MOD_PRC

/*!
 * \stuct usb. Top structure for the USB module.
 */
struct usbStr {
   usbDev_t dev;        /*!< USB Interface device.*/
   usbCtr_t ctr;        /*!< Control Transfer logic.*/
   usbFun_t fun;        /*!< USB Module functions.*/
   char     enb;        /*!< USB Module enabled/disagled state.*/
   char     fsm;        /*!< USB Module Finite State Machine.*/
   void     (*inpPck()) /*!< USB Module function pointer, handling the input  packet.*/
   void     (*outPck()) /*!< USB Module function pointer, handling the output packet.*/
   void     (*stpPck()) /*!< USB Module function pointer, handling the setup  packet.*/
};

typedef struct usbStr usb_t;
