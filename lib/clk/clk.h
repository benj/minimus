// ===========================================================================
//  File          :    clk.h
//  Entity        :    clk
//  Purpose       :    AVR Clock library module.
//  Documentation :   
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 16-Aug-2012
// ===========================================================================

#include "../gpio/gpio.h"

/*! Macro for getting the RC oscillator fuse bits.*/
#define clkGetFusRC() (CLKSEL0 >> RCSUT0)

/*! Macro for getting the External Oscillator/Low Power Oscillator fuse bits.*/
#define clkGetFusExt() ((CLKSEL0 >> EXSUT0) & 0x3)

/*! Macro for enabling the RC oscillator.*/
#define clkEnbRc() SETBIT(CLKSEL0, RCE)

/*! Macro for enabling the external / low power oscillator.*/
#define clkEnbExt() SETBIT(CLKSEL0, EXTE)

/*! Macro for disabling the RC oscillator.*/
#define clkDsbRc() SETBIT(CLKSEL0, RCE)

/*! Macro for disabling the external / low power oscillator.*/
#define clkDsbExt() SETBIT(CLKSEL0, EXTE)

/*! Macro for selecting the External / low power oscillator as the CPU clock.*/
#define clkSelExt() SETBIT(CLKSEL0, CLKS)

/*! Macro for selecting the internal RC osccillator as the CPU clock.*/
#define clkSelRc()  CLRBIT(CLKSEL0, CLKS)

/*! Macros for getting the status of the clocks. */
#define CLK_RUN 1 /*!< Definition of a running clock.*/
#define CLK_STP 0 /*!< Definition of a halted clock.*/

/*! Macro for getting the status of the RC clock.*/
#define clkGetStaRc() GETBITVAL(CLKSTA, RCON)

/*! Macro for getting the status of the external clock.*/
#define clkGetStaExt() GETBITVAL(CLKSTA, EXTON)

/*! Macro for setting the oscillator callibration register.*/
#define clkSetCal(cal) (OSCCAL = cal)

/*! Macro for setting the system clock precaler.*/
#define CLK_SYS_PRS_001 0
#define CLK_SYS_PRS_002 1
#define CLK_SYS_PRS_004 2
#define CLK_SYS_PRS_008 3
#define CLK_SYS_PRS_016 4
#define CLK_SYS_PRS_032 5
#define CLK_SYS_PRS_064 6
#define CLK_SYS_PRS_128 7
#define CLK_SYS_PRS_256 8
#define clkSetSysPrs(prs) \
	 CLKPR = 0b10000000; \
	(CLKPR = (prs & 0xF))

/*! Macro for clearing the precaling.*/
#define clkPrsClr() clkSetSysPrs(CLK_SYS_PRS_001)

/*! Macro for enabling the PLL.*/
#define clkEnbPll() SETBIT(PLLCSR, PLLE)

/*! Macro for disabling the PLL.*/
#define clkDsbPll() (PLLCSR &= ~0x3)

/*! Macro for setting the prescaler values.*/
#define clkSetPllPrs5() SETBIT(PLLCSR, DIV5)
#define clkSetPllPrs3() SETBIT(PLLCSR, DIV3)
#define clkSetPllPrs2() SETBIT(PLLCSR, PINDIV)
#define clkSetPllPrs1() CLRBIT(PLLCSR, PINDIV)

/*! Macro for geting the value of the lock for the PLL.*/
#define clkGetStaPll() GETBITVAL(PLLCSR, PLOCK)

/*! Function for switching from external to RC clock.*/
void clkSwtExt2Rc();

/*! Function for switching from RC to external clock.*/
void clkSwtRc2Ext();

/*! Function for setting up the USB clock configuration.*/
void clkSetUsb();
