// ===========================================================================
//
//  File          :    clk.c
//  Entity        :    clk
//  Purpose       :    AVR Clock library module.
//  Documentation :   
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 16-Aug-2012
// ===========================================================================

#include "../lib.h"

/*! Function for switching from external to RC clock.*/
void clkSwtExt2Rc() {
   clkDsbPll();
   clkEnbRc();
   while(!clkGetStaRc());
   clkSelRc();
   clkDsbExt();
}

/*! Function for switching from RC to external clock.*/
void clkSwtRc2Ext() {
   clkEnbExt();
   while(!clkGetStaExt());
   clkEnbPll();
   while(!clkGetStaPll());
}

/*! Function for setting up the USB clock configuration.*/
void clkSetUsb() {
   clkSwtRc2Ext();
   clkSetSysPrs(CLK_SYS_PRS_002);
   clkSetPllPrs2();
}
