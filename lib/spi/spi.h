// ===========================================================================
//  File          :    spi.h
//  Entity        :    spi
//  Purpose       :    Serial Parallele Interface library module.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 04-Aug-2012
//  File history    :
// ===========================================================================

#define ENB 1
#define DSB 0

/* Macro to enable the SPI Interrupt. */
#define spiIntEnbMod(mod) \
   SETBITVAL(SPCR, SPIE, mod)

/* Macro to enable the SPI */
#define setSpiMod(mod) \
   SETBITVAL(SPCR, SPE, mod)

// SPI roles defintions
#define ROL_MST 1
#define ROL_SLV 0

/* Macro for defining master/slave role */
#define setRol(rol) \
   SETBITVAL(SPCR, MSTR, rol)

// Port definitions
#define SPI_GPIO_ADD GPIO_B_ADD
#define SPI_MIS_ADD  SPI_GPIO_ADD + 3
#define SPI_MOS_ADD  SPI_GPIO_ADD + 2
#define SPI_CLK_ADD  SPI_GPIO_ADD + 1
#define SPI_SS_ADD   SPI_GPIO_ADD + 0

/* Macro for ports setting */
#define setPrt(rol) \
   if (rol == ROL_MST) {\
      gpioSetDir(SPI_MOS_ADD, OUT);\
      gpioSetDir(SPI_CLK_ADD, OUT);\
      gpioSetDir(SPI_SS_ADD,  OUT);\
      gpioSetDir(SPI_MIS_ADD, IN); \
   }\
   else if (rol == ROL_SLV) {\
      gpioSetDir(SPI_MOS_ADD, IN); \
      gpioSetDir(SPI_CLK_ADD, IN); \
      gpioSetDir(SPI_SS_ADD,  IN); \
      gpioSetDir(SPI_MIS_ADD, OUT);\
   }\

// Bit transmission order definitions
#define ORD_LSB_FRS 1
#define ORD_LSB_LST 0

/* Macro for setting the data order */
#define setDatOrd(ord) \
   SETBITVAL(SPCR, DORD, ord)


// Clock polarity definitions
#define CLK_POL_HGH 1
#define CLK_POL_LOW 0

/* Macro for defining clock polarity */
#define setClkPol(pol) \
   SETBITVAL(SPCR, CPOL, pol)

// Clock phase definitions
#define CLK_PHS_LED 0
#define CLK_PHS_STR 1

/* Macro for defining clock phase */
#define setClkPhs(phs) \
   SETBITVAL(SPCR, CPHA, phs)

// Transmission rate definitions
#define TRM_RTE_F2   4
#define TRM_RTE_F4   0
#define TRM_RTE_F8   5
#define TRM_RTE_F16  1
#define TRM_RTE_F32  6
#define TRM_RTE_F64  2
#define TRM_RTE_F128 3

/* Macro for setting the transmission rate */
#define setTrmRte(TRM_RTE) \
   SETBITVAL(SPCR, SPR0, (TRM_RTE & 0b1));\
   SETBITVAL(SPCR, SPR1, ((TRM_RTE >> 1) & 0b1));\
   SETBITVAL(SPSR, SPI2X, ((TRM_RTE >> 2) & 0b1));

/* Macro for transmitting a byte */
#define spiTx(bte, rol) \
   if (rol == ROL_MST) {\
      gpioSet(SPI_SS_ADD);\
   }\
   SPDR = bte

/* Macro for receiving a byte */
#define spiRx(bte) \
   SPDR + 0


/* Macro for setting the master / slave configuration */
#define setSpi(rol, ord, clkPol, clkPhs, trmRte) \
	setSpiMod(ENB);\
   sei();\
	spiIntEnbMod(ENB);\
	setDatOrd(ord);\
	setRol(rol);\
	setClkPol(clkPol);\
	setClkPhs(clkPhs);\
	setTrmRte(trmRte);\
	setPrt(rol);
