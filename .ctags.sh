#! /bin/bash
# ===========================================================================
#  File          :    .ctags.sh
#  Entity        :    minimus
#  Purpose       :    Script for generating the ctags under the minimus development folder.
#  Documentation :   
#  Notes         :
# ===========================================================================
#  Design Engineer : Angelos Spanos (TE)
#  Creation Date   : 07-Aug-2012
# ===========================================================================

# Remove the previous tags file
rm -f ./tags

# Generate the ctags from this project.
ctags -R ./*

# Append the ctags from the AVR IO header for atmega32u2.
ctags --append /usr/lib/avr/include/avr/iom32u2.h

# Append the ctags from the utilities library.
ctags --append /usr/lib/avr/include/util/*

# Append the ctags from the interrupts library.
ctags --append /usr/lib/avr/include/avr/interrupt.h
