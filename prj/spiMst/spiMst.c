/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
      tmr
      spi
   The program needs two boards in order to be run.
   This source code provides the implementation of the demo for the master.
   The master enables the communication and sends the phrase 'You are my slave!'
   and the slave replies. 'Yes master...'
*/
#include "avr/io.h"
#include "avr/wdt.h"
#include "avr/interrupt.h"
#include "../../lib/lib.h"

// Configuration options.
#define ROL ROL_MST
#define ORD ORD_LSB_FRS
#define CLK_POL CLK_POL_HGH
#define CLK_PHS CLK_PHS_LED
#define TRM_RTE TRM_RTE_F128

// Led Toggle Delay
#define LED_TGL_EFF_DEL 40000

void sndChr() {
   spiTx('M', ROL);
}

// SPI Serial Transfer Complete Interrupt Service Routine
ISR(SPI_STC_vect) {
   gpioSet(SPI_SS_ADD);
   ledSet(LED_RED);
   if (spiRx() == 'S') {
      ledSet(LED_BLU);
   }
   ledClr(LED_RED);
   sndChr();
}

int main(void) {
   // Disable the watchdog timer. 
   wdtDsb();


   //Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);

   //Initialize button
   butIni();

   //Set-up a master configuration
   setSpi(ROL, ORD, CLK_POL, CLK_PHS, TRM_RTE);
   sndChr();

   // Wait for interrupts
   while (1) {
      sndChr();
   }
   return 0;
}
