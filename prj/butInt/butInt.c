/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
      The button is connected to the INT7
*/

#include "avr/io.h"
#include "avr/wdt.h"
#include "avr/interrupt.h"
#include "../../lib/lib.h"

//Interrupt-serviced flag
int irqFlg = 0;

//Interrupt service routine for the button
ISR(INT7_vect) {
   ledSet(LED_BLU);
   ledClr(LED_RED);
   irqFlg = 1;
}

int main(void) {
   // Disable the watchdog timer. 
   wdtDsb();

   //Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);

   //Initialize the button.
   butIni();
   butEnbIrq();

   // Select the turned on LED accurding to the button pressed.
   while (1) {
      ledSet(LED_RED);
      ledClr(LED_BLU);
      while(irqFlg == 0);
      irqFlg = 0;
      while(irqFlg == 0);
   }
   return 0;
}
