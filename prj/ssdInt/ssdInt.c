// ===========================================================================
//  File          :    ssdInt.c
//  Entity        :    ssdInt
//  Purpose       :    Source code for the seven segment display controlled 
//                     by the timer interrupt.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 06-Aug-2012
//  File history    :
// ===========================================================================

#include "../../lib/lib.h"
#include "avr/io.h"
#include "avr/interrupt.h"

extern char *ssdVal;

int  main() {
	// Disable the watch dog timer.
	wdtDsb();

	//Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);

   //Initialize the SSD.
   ssdIni();
   
   //Initialize the timer.
   //// Set the clock source to be the core clock.
   setTmrClk(TMR_CLK64);

   ////Set the TOP value for the two timers.
   setCmpVal(CMP_OUT_A, 63);

   ////Enable the interrupt.
   sei();
   enbTmrInt(CMP_OUT_A);
   
   //// Reset the timer.
   rstTmr();

   // Initialize the pointer for the value of the SSD.
   ssdVal = &OCR0A;

   // Basic configuration finished.
   while(1);
   return 0;
}
