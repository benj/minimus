// ===========================================================================
//  File          :    ssdTmr.c
//  Entity        :    ssdTmr
//  Purpose       :    This program increments the SSD per second.
//  Documentation :   
//  Notes         :
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 05-Aug-2012
//  File history    :
// ===========================================================================

#include "avr/io.h"
#include "util/delay.h"
#include "../../lib/lib.h"

int main() {
   char hexCnt;
   char secCnt;
   // Disable the watchdog timer. 
   wdtDsb();

   //Initialize the SSD.
   ssdIni();
   ssdEnbRgtDgt();
   for (hexCnt  = 0; hexCnt <= 1; hexCnt++) {
      // Delay 10 seconds.
      for (secCnt = 0; secCnt < 0xF; secCnt++) {
         ssdPrnNbl(secCnt);
         _delay_ms(1000);
      }
   }

   // Indicate program end.
   while (1) {
      ssdPrnNbl(secCnt);
   }

   return 0;
}
