/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
      tmr
      int
*/
#include "avr/io.h"
#include "avr/wdt.h"
#include "avr/interrupt.h"
#include "../../lib/lib.h"
#define TOP_VAL 0xFF

//Timer Compare Interrupt service routine
ISR(TIMER0_COMPA_vect) {
   ledSet(LED_RED);
   ledClr(LED_BLU);
}

//Timer Compare Interrupt service routine
ISR(TIMER0_COMPB_vect) {
   ledSet(LED_BLU);
   ledClr(LED_RED);
}


int main(void) {
   // Disable the watchdog timer. 
   wdtDsb();

   //Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);

   //Initialize the button.
   butIni();

   //Initialize the timer.
   ////Set the clock source to be the core clock with prescale 1024.
   setTmrClk(TMR_CLK1024);
  
   ////Enable the output from the two timers
   enbTmrOut(CMP_OUT_A);
   enbTmrOut(CMP_OUT_B);

   ////Set the output to toggle
   togTmrOut(CMP_OUT_A);
   togTmrOut(CMP_OUT_B);

   ////Set the TOP value for the two timers
   setCmpVal(CMP_OUT_A, TOP_VAL);
   setCmpVal(CMP_OUT_B, TOP_VAL);
   
   ////Set the waveform generation mode to normal.
   setWvfMod(MOD_CTC);
   ////Set the interrupt mask and flag register
   sei();
   enbTmrInt(CMP_OUT_A);
   enbTmrInt(CMP_OUT_B);

   ////Reset the timers
   rstTmr();

   // Basic configuration finished.
   while (1);
   return 0;
}
