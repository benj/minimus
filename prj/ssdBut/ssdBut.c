/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
      ssd
  This binary waits on the button to be pressed and increases the value presented
  on the seven segment display.
*/
#include "avr/wdt.h"
#include "../../lib/lib.h"
#include "util/delay.h"

int main(void) {
   char val = 1; /*!< Value of the seven segment display */

   // Disable the watchdog timer. 
   wdtDsb();

   // Initialize the Seven Segment Display board.
   ssdIni();

   while(1) {
      // Print the current value
      ssdPrn(val);
      
      //Increment the count on a clean button press.
      if (butGetCln()) {
         val++;
         // Wait until the button has been released.
         while (butGetCln())
            ssdPrn(val);

      }
   }
}
