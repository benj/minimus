/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
*/

#include "avr/io.h"
#include "util/delay.h"
#include "../../lib/lib.h"

int main(void) {   
   // Disable the watchdog timer. 
   wdtDsb();

   //Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);
   ledSet(LED_RED);
   ledClr(LED_BLU);

   //Initialize the button.
   butIni();

   // Select the turned on LED accurding to the button pressed.
   while (1) {
      //Check the status of the button.
         // Turn on only the blue LED.
      ledTgl(LED_RED);
      ledTgl(LED_BLU);
      _delay_ms(1000);
   }
   return 0;
}

