/*
   Program that demonstrates the functionality of the following libraries
      wdt
      rot
      led
      gpio
      but
      tmr
   The program needs two boards in order to be run. 
   The boards listen to PINT7.
   After initialization the boards output an interrupt on PINT2.
   On a received interrupt the boards _delay_usay and output a new interrupt.
*/
#include "avr/io.h"
#include "avr/wdt.h"
#include "avr/interrupt.h"
#include "util/delay.h"
#include "../../lib/lib.h"

#define DEL_TME 10000
#define OUT_PRT GPIO_D_ADD
#define OUT_PRT_FLD 7
#define OUT_ADD OUT_PRT + OUT_PRT_FLD

void trn() {
   ledSet(LED_RED);
   gpioSet(OUT_ADD);
   _delay_us(DEL_TME);
   ledClr(LED_RED);
   gpioSet(OUT_ADD);
   _delay_us(DEL_TME);
}

//Interrupt service routine for the PCINT2
ISR(INT2_vect) {
   ledSet(LED_BLU);
   _delay_us(DEL_TME/8);
   ledClr(LED_BLU);
   _delay_us(DEL_TME/8);
}

int main(void) {
   // Disable the watchdog timer. 
   wdtDsb();

   //Initialize the LEDs.
   ledIni(LED_BLU);
   ledIni(LED_RED);

   //Initialize button
   butIni();

   //Enable the PCINT2.
   //setChnIrq(PCPB2);
   setIrqMod(PIPD2, EDG_RIS, ENB);

   //Set the direction of the output port 
   gpioSetDir(OUT_ADD, OUT);
   gpioClr(OUT_ADD);

   // Wait for interrupts
   while (1) {
      trn();
   }
   return 0;
}
