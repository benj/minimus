// ===========================================================================
//  File          :    usbPrj.c
//  Entity        :    usbPrj
//  Purpose       :    USB Project. Testbench for the USB firmware development.
//  Notes         :    The behaviour of the module is the following.
//                        1. on lsusb, the USB device returns AVR USB development.
//                        2. on SUSPI,     only the RED  LED is lit.
//                        3. on non-SUSPI, only the BLUE LED is lit.
// ===========================================================================
//  Design Engineer : Angelos Spanos (TE)
//  Creation Date   : 08-Aug-2012
// ===========================================================================

#include "../../lib/lib.h"

int main() {

   usbPowOff();
   usbPowOn();
   if (usbChkMem()) {
      ledIni(LED_BLU);
      ledSet(LED_BLU);
   }
   return 0;
}
