/*	Sample program for Olimex AVR-USB-162 with AT90USB162 processor
 *	Blinks the led using a simple delay loop.
 *	Compile with AVRStudio+WinAVR (gcc version 3.4.6)
 */

#include "avr/io.h"

void PORT_Init()
{
	PORTB = 0b00000000;
	DDRB = 0b00000000;

	PORTC = 0b00000000;
	DDRC = 0b00000000;

	PORTD = 0b00000000;
	DDRD = 0b01100000;		//set led as output
}

int i;

int main(void)
{	
	asm("WDR");						//Watchdog Timer Reset
	PORT_Init();
	MCUSR= ~(1<<WDRF);
	WDTCSR = (1<<WDCE) | (1<<WDE);
	WDTCSR = 0x00;
	WDTCKD = 0x00;

	while (1)
	{
	   asm("WDR");						//Watchdog Timer Reset
		if (!(PIND & 0b10000000)) {
			PORTD = PORTD & 0b10111111;
			PORTD = PORTD | 0b00100000;
			for (i = 10000; i; i--);
		
			PORTD = PORTD & 0b11011111;
			PORTD = PORTD | 0b01000000;
			for (i = 10000; i; i--);
		}
		else {
			PORTD = PORTD & 0b10111111;
			PORTD = PORTD | 0b00100000;
			for (i = 25000; i; i--);
		
			PORTD = PORTD & 0b11011111;
			PORTD = PORTD | 0b01000000;
			for (i = 25000; i; i--);
		}

	}
	return 0;
}

