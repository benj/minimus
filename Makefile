LIB_NME=atmega32u2
LIB_CMP=led but gpio rot wdt tmr irq spi ssd clk usb

PRJ=butLED butInt tmrDmo pinPon spiMst spiSlv ssdBut ssdDel ssdInt clkPrj usbPrj

all: $(LIB_CMP) $(PRJ)
lib: $(LIB_CMP)
prj: $(PRJ)

.PHONY: library
$(LIB_CMP):
	$(MAKE) -C lib/$@ $@.o

.PHONY: projects
$(PRJ):$(LIB_CMP)
	$(MAKE) -C prj/$@ $@.hex

clean:
	for lib_cmp in $(LIB_CMP); do \
		$(MAKE) -C lib/$$lib_cmp clean; \
	done
	for prj in $(PRJ); do \
		$(MAKE) -C prj/$$prj clean; \
	done
	rm -f lib/*.a
